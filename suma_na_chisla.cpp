#include <iostream>

using namespace std;

int main()
{
    int cnt;
    double sum = 0;
    cin >> cnt;
    for(int i = 0; i < cnt; i++)
    {
        double number;
        cin >> number;
        sum += number;
    }

    cout << sum << endl;

    return 0;
}
