#include <iostream>
#include <string>

using namespace std;

int main()
{
    string fname, lname;

    cout << "Enter your name: " << "\n";
    cin >> fname >> lname;

    cout << lname << ", " << fname;

    return 0;
}
