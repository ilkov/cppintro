#ifndef GAME_H
#define GAME_H
#include "Graphics.h"
//#include "Sprite.h"
#include "Player.h"
#include "Level.h"

class Game
{
public:
	Game();
	~Game();

private:
	void gameLoop();
	void draw(Graphics &graphics);
	void update(float elapsedTime);

	Player _player;

	Level _level;
};

#endif