#ifndef INPUT_H
#define INPUT_H
#include <SDL.h>
#include <map>
#include <vector>

class Input
{
public:
	void beginNewFrame();
	void keyUpEvent(const SDL_Event& event);
	void keyDownEvent(const SDL_Event& event);

	bool wasKeyPressed(SDL_Scancode key);
	bool wasKeyReleased(SDL_Scancode key);
	bool isKeyHeld(SDL_Scancode key);
	SDL_Scancode getLastPressedKey();
	void setLastKeyPressed(SDL_Scancode pressedKey);
	void clear();
private:
	std::map<SDL_Scancode, bool> _heldKeys;
	std::map<SDL_Scancode, bool> _pressedKeys;
	std::map<SDL_Scancode, bool> _releasedKeys;
	SDL_Scancode lastKey;
};

#endif