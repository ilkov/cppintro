#include "Player.h"
#include "Graphics.h"
#include "AnimatedSprite.h"
#include <SDL_mixer.h>

namespace player_contants
{
	const float WALK_SPEED = 0.1f;
}

Player::Player() {}

Player::Player(Graphics &graphics, float x, float y) : AnimatedSprite(graphics, "Content/Sprites/tank.png", 0, 0, 16, 16, x, y, 100)
{
	graphics.loadImage("Content/Sprites/tank.png");

	setUpAnimations();
	playAnimation("DriveDown");
}

void Player::setUpAnimations()
{
	addAnimation(1, 0, 0, "IdleUP", 16, 16, Vector2(0, 0));
	addAnimation(1, 2, 0, "IdleLeft", 16, 16, Vector2(0, 0));
	addAnimation(1, 4, 0, "IdleDown", 16, 16, Vector2(0, 0));
	addAnimation(1, 6, 0, "IdleRight", 16, 16, Vector2(0, 0));


	addAnimation(2, 0, 0, "DriveUP", 16, 16, Vector2(0, 0));
	addAnimation(2, 2, 0, "DriveLeft", 16, 16, Vector2(0, 0));
	addAnimation(2, 4, 0, "DriveDown", 16, 16, Vector2(0, 0));
	addAnimation(2, 6, 0, "DriveRight", 16, 16, Vector2(0, 0));

}

void Player::animationDone(std::string currentAnimation) {}

const float Player::getX() const
{
	return _x;
}

const float Player::getY() const
{
	return _y;
}

void Player::moveLeft()
{
	_dx = -player_contants::WALK_SPEED;
	/*if (_x + _dx <= 0)
	{
		_dx = 0;
	}*/
	playAnimation("DriveLeft");
	_facing = LEFT;
}

void Player::moveRight()
{
	_dx = player_contants::WALK_SPEED;
	playAnimation("DriveRight");
	_facing = RIGHT;
}

void Player::moveDown()
{
	_dy = player_contants::WALK_SPEED;
	playAnimation("DriveDown");
	_facing = DOWN;
}

void Player::moveUp()
{
	_dy = -player_contants::WALK_SPEED;
	/*if (_y + _dy <= 0)
	{
		_dy = 0;
	}*/
	playAnimation("DriveUP");
	_facing = UP;
}

void Player::stopMoving()
{
	_dx = 0.0f;
	_dy = 0.0f;

	playAnimation(_facing == RIGHT ? "IdleRight" : "IdleLeft");
	playAnimation(_facing == UP ? "IdleUP" : "IdleDown");
}

bool Player::loadSounds()
{

	//The music that will be played 
	Mix_Music *music = NULL;

	//Load the music 
	music = Mix_LoadMUS( "Battle City SFX (16).wav" ); 
	
	//If there was a problem loading the music 
	if( music == NULL )
	{ 
		return false; 
	}
	//If everything loaded fine 
	return true;
}

// Handles Collisions with all tiles the player is colliding with
void Player::handleTileCollisions(std::vector<Rectangle> &others)
{
	// Figure out what side the collision happened on and move the player accordingly
	for (int i = 0; i < others.size(); i++)
	{
		sides::Side collisionSide = Sprite::getcollisionSide(others.at(i));
		if (collisionSide != sides::NONE)
		{
			switch (collisionSide)
			{
			case sides::TOP:
				_y = others.at(i).getBottom() + 1;
				_dy = 0;
				break;
			case sides::BOTTOM:
				_y = others.at(i).getTop() - _boundingBox.getHeight() - 1;
				_dy = 0;
				break;
			case sides::LEFT:
				_x = others.at(i).getRight() + 1;
				break;
			case sides::RIGHT:
				_x = others.at(i).getLeft() - _boundingBox.getWidth() - 1;
				break;
			}
		}
	}
}

void Player::update(float elapsedTime)
{
	prevX = _x;
	prevY = _y;
	// Move by _dx
	_x += _dx * elapsedTime;

	// Move by _dy
	_y += _dy * elapsedTime;

	AnimatedSprite::update(elapsedTime);
}

void Player::draw(Graphics &graphics)
{
	AnimatedSprite::draw(graphics, _x, _y);
}

void Player::toPrev() 
{
	_x = prevX;
	_y = prevY;
}

