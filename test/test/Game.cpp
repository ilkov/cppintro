#include <SDL.h>
#include "Game.h"
#include "Input.h"
#include <algorithm>
#include <string>
#include <SDL_mixer.h>
using namespace std;
/*	Game class
*	Holds all information about the main game loop
*/

namespace
{
	const int FPS = 50;
	const int MAX_FRAME_TIME = 5 * 1000 / FPS;
}

Game::Game()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	gameLoop();
}
Game::~Game()
{

}
void Game::gameLoop()
{
	Graphics graphics;
	Input input;
	SDL_Event event;

	_player = Player(graphics, 5, 5);

	_level = Level("Map2", Vector2(100, 100), graphics);

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

	int LAST_UPDATE_TIME = SDL_GetTicks();  //SDL_GetTick -> gets the number of milliseconds since the SDL libraly was initialized

	Mix_Chunk *playerMove = Mix_LoadWAV("Battle City SFX (16).wav");

	while (true)
	{
		input.beginNewFrame();  //resets pressed keys and released keys at the beginning of every frame

		if (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.repeat == 0)  //key.repeat == 0 -> makes sure that you are not holding down a key
				{
					input.keyDownEvent(event);
				}
			}
			else if (event.type == SDL_KEYUP)
			{
				input.keyUpEvent(event);
			}
			else if (event.type == SDL_QUIT)
			{
				return;
			}
		}
		if (input.wasKeyPressed(SDL_SCANCODE_ESCAPE) == true)  //if ESC button is pressed, quit the game
		{
			return;
		}
		else if (input.isKeyHeld(SDL_SCANCODE_LEFT) == true)
		{
			_player.moveLeft();
		}
		else if (input.isKeyHeld(SDL_SCANCODE_RIGHT) == true)
		{
			_player.moveRight();
		}
		else if (input.isKeyHeld(SDL_SCANCODE_DOWN) == true)
		{
			_player.moveDown();
			Mix_PlayChannel(-1, playerMove, 0);
		}
		else if (input.isKeyHeld(SDL_SCANCODE_UP) == true)
		{
			_player.moveUp();
		}
		
		if (!input.isKeyHeld(SDL_SCANCODE_LEFT) && !input.isKeyHeld(SDL_SCANCODE_RIGHT) && !input.isKeyHeld(SDL_SCANCODE_DOWN) && !input.isKeyHeld(SDL_SCANCODE_UP))
		{
			_player.stopMoving();
		}
		const int CURRENT_TIME_MS = SDL_GetTicks();  //Holds the millisecond value that took for the gameloop to complete
		int ELAPSED_TIME_MS = CURRENT_TIME_MS - LAST_UPDATE_TIME;  //How long this current frame took
		update(std::min(ELAPSED_TIME_MS, MAX_FRAME_TIME));  //If this frame took less than our maximum time, we are going to use that time. If it took more than that, use our maximum time.
		//It can't go higher than 50 FPS

		LAST_UPDATE_TIME = CURRENT_TIME_MS;  //Start the initialization again, before the loop begins a new cicle
	
		draw(graphics);
	}
}
void Game::draw(Graphics &graphics)
{
	graphics.clear();

	_level.draw(graphics);
	_player.draw(graphics);

	graphics.flip();
}
void Game::update(float elapsedTime)
{
	_player.update(elapsedTime);

	_level.update(elapsedTime);

	// Check collisions
	std::vector<Rectangle> others;
	if ((others = _level.checkTileCollision(_player.getBoundingBox())).size() > 0 )
	{
		// Player collided with at least one tile. Handle it
		//-- _player.handleTileCollisions(others);
		_player.toPrev();
	}
}