#ifndef PLAYER_H
#define PLAYER_H
#include "Globals.h"
#include "AnimatedSprite.h"
using namespace std;

class Graphics;

class Player : public AnimatedSprite
{
public:
	//--SM
	void toPrev();
	//--SM
	Player();
	Player(Graphics &graphics, float x, float y);
	void draw(Graphics &graphics);
	void update(float elapsedTime);

	// Moves the player left by _dx
	void moveLeft();

	// Moves the player right by negative _dx
	void moveRight();

	// Moves the player up by _dy
	void moveUp();

	// Moves the player down by negtive _dy
	void moveDown();


	// Stops moving the player and idle
	void stopMoving();

	virtual void animationDone(std::string currentAnimation);
	virtual void setUpAnimations();

	void handleTileCollisions(std::vector<Rectangle> &others);

	const float getX() const;
	const float getY() const;

	bool loadSounds();
	void close();

private:
	float _dx, _dy;
	//--SM
	float prevX, prevY;
	//--SM

	


	Direction _facing;
};

#endif