#ifndef SPRITE_H
#define SPRITE_H
#include "Graphics.h"
#include "Rectangle.h"
#include "Globals.h"
#include <SDL.h>
#include <string>

class Graphics;
using namespace std;

/* Spirte class
   Holds all information for individual sprites
*/

class Sprite
{
public:
	Sprite();
	Sprite(Graphics &graphics, const std::string &filePath, int sourceX, int sourceY, int width, int height, float posX, float posY);
	virtual ~Sprite();
	virtual void update();
	void draw(Graphics &graphics, int x, int y);

	const Rectangle getBoundingBox() const;
	const sides::Side getcollisionSide(Rectangle &other) const;

private:


protected:
	SDL_Rect _sourceRect;
	SDL_Texture* _spriteSheet;

	Rectangle _boundingBox;

	float _x, _y;
};

#endif