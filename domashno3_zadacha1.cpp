#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main()
{
    vector <double> v1;
    vector <double> v2;
    int numbers_count;
    double input_v1, input_v2;
    double happy;
    cin >> numbers_count;

    for(int i=0; i < numbers_count; i++)
    {
        cin >> input_v1;
        v1.push_back(input_v1);

        cin >> input_v2;
        v2.push_back(input_v2);

        happy = (v1[0]/v2[0]) - (v1[1]/v2[1]) - (v1[2]/v2[2]) - (v1[3]/v2[3]) - (v1[4]/v2[4]) - (v1[5]/v2[5]) - (v1[6]/v2[6]) - (v1[7]/v2[7]);
    }
    cout << fixed << setprecision(4) << happy << endl;

    return 0;
}
