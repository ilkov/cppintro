#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main()
{
    vector <double> v1;
    vector <double> v2;
    int numbers_count;
    double input_v1, input_v2;
    double happy;
    int pos;
    cin >> numbers_count;

    for(int i=0; i < numbers_count; i++)
    {
        pos = i;
        cin >> input_v1;
        v1.push_back(input_v1);
    }
        for(int j=0; j < numbers_count; j++)
    {
        cin >> input_v2;
        v2.push_back(input_v2);

       happy = (v1[pos]*v2[j]) + ((v1[pos] + 1)) * ((v2[j] + 1));
    }
    cout << fixed << setprecision(4) << happy << endl;

    return 0;
}
