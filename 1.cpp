/***
FN:F73855
PID:1
GID:3
*/ 

#include <iostream>
#include <string>
using namespace std;
 
class Estate
{
private:
	string name;
	string type;
	int bedrooms;
public:
	Estate(const string &name, const string &type, int bedrooms) : name(name), type(type), bedrooms(bedrooms)
	{
	}
	void setName(string hname)
	{
		name = hname;
	}
	
	const string & getName() const
	{
		return name;
	}
	
	void setType(string htype)
	{
		type = htype;
	}

	const string & getType() const
	{
		return type;
	}

	void setBedrooms(int hbedrooms)
	{
		bedrooms = hbedrooms;
	}

	int getBedrooms()
	{
		return bedrooms;
	}
	
	void print()
	{
		cout << getName() << " (" << getType() << ") " << endl << getBedrooms() << " bedrooms." << endl;
	}
};
	void printEstateInfo(Estate *se)
	{
		cout << se->getType() << ": " << se->getName() << ", " << se->getBedrooms() << endl;
	}

int main()
{
	Estate* es = new Estate("46 Harbour Street Wollongong", "Apartment", 3);
	/*
	Should print:
	Apartment: 46 Harbour Street Wollongong, 3
	*/
	printEstateInfo(es);

	/*
	Should print:
	46 Harbour Street Wollongong (Apartment)
	3 bedrooms.
	*/
	es->print();

	es->setName("Harbour Street Wollongong");
	es->setBedrooms(3);
	es->setType("House");

	/*
	Should print:
	House: Harbour Street Wollongong, 3
	*/
	printEstateInfo(es);

	/*
	Should print:
	Harbour Street Wollongong (House)
	3 bedrooms.
	*/
	es->print();

	delete es;
	system("pause");
	return 0;
}