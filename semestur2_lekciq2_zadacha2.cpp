#include <iostream>
#include <vector>
#include <limits>

using namespace std;

void prn (const vector<double> &v);
double avg_nice (const vector<double> &prices);

int main()
{
    int n;
    cin >> n;
    vector <double> prices;

    for (int i = 0; i<n; i++)
    {
        double p;
        cin >> p;

        prices.push_back(p);
    }

    cout << avg_nice(prices) << endl;

    return 0;
}
void prn (const vector<double> &v)
{
    for (int i =0; i<v.size(); i++)
    {
        cout << v[i] << endl;
    }
}
double avg_nice (const vector<double> &prices)
{
    double maxPrice = numeric_limits<double>::min();
    double minPrice = numeric_limits<double>::max();
    for(int i=0; i<prices.size(); i++)
    {
        double p = prices[i];
        minPrice = min(minPrice, prices[i]);
        maxPrice = max(maxPrice, prices[i]);
    }
    double sum = 0.0;
    int cnt = 0;
    for(int i =0; i<prices.size(); i++)
    {
        if(prices[i] != maxPrice && prices[i] != minPrice)
        {
            sum += prices[i];
            cnt ++;
        }
    }
    return sum / cnt;

}
