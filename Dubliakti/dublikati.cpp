#include <iostream>

using namespace std;

int main()
{
    double number1, number2, number3;

    cout << "Enter your 3 numbers: " << "\n";
    cin >> number1 >> number2 >> number3;

    if ((number1 == number2) || (number1 == number3) || (number2 == number3)) {
        cout << "There is a duplicate number: " << number1 || number2 || number3;
    }
    else{
        cout << "There aren't any duplicates ";
    }

    return 0;
}
