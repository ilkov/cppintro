/***
FN:F00000
PID:2
GID:3
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>

using namespace std;

class Worker
{
private:
	string name;
	double pay;
public:
	Worker(const string &name, double pay) : name(name), pay(pay)
	{
	}
	virtual double getWeeklySalary(int hours) = 0;
	const string & getName() const
	{
		return name;
	}

	double getHourlyPay()
	{
		return pay;
	}
};

class SalariedWorker : public Worker
{
public:
	SalariedWorker(const string &name, double pay) : Worker(name, pay)
	{
	}
	
	double getWeeklySalary(int hours)
	{
		double pay = getHourlyPay();
		if (hours > 40)
			pay *= 1.2;
		else if (hours < 40)
			pay *= 0.5;

		return hours*pay;
	}
};

class Consultant : public Worker
{
public:
	Consultant(const string &name, double pay) : Worker(name, pay)
	{
	}
	double getWeeklySalary(int hours)
	{
		hours = min(hours, 20);
		return getHourlyPay() * hours;
	}
};

int main()
{
	int n;
	cin >> n;
	vector<Worker*> workers(n);
	for (int i = 0; i < n; i++)
	{
		string type, name;
		double pay;
		cin >> type >> name >> pay;
		if (type == "s")
			workers[i] = new SalariedWorker(name, pay);
		else if (type == "c")
			workers[i] = new Consultant(name, pay);

	}
	
	int q;
	cin >> q;
	for (int i = 0; i < q; i++)
	{
		int idx, hours;
		cin >> idx >> hours;
		/*idx--; //1->0*/
		Worker *worker = workers[idx - 1];
		cout << worker ->getName() << " worked " << hours <<  " hours and should receive " << worker->getWeeklySalary(hours) << endl;

	}

	return 0;
}