/* ако натисна CTRL+D, за да спра входа, след последното число, на новия ред
това спиране на входа ми го брои като допълнително число(същото като последното въведено)
към countt, а ако спра входа на същия ред, на който е и последното число, не ми го брои.
Не знам как да го оправя, затова съм го оставил така, с тази бележка.*/
#include <iostream>
#include <climits>

using namespace std;

int main()
{
    int min_num = INT_MAX;
    int max_num = INT_MIN;
    int input;
    int countt = 0;
    int sum = 0;
    double avg;

    while (cin >> input)
    {
    min_num = min(input, min_num);
    max_num = max(input, max_num);

    countt++;
    sum += input;
    avg = sum / countt;

    }
    cout << "Min: " << min_num << endl;
    cout << "Max: " << max_num << endl;
    cout << "Count: " << countt << endl;
    cout << "Sum: " << sum << endl;
    cout << "Average: " << avg << endl;


    return 0;
}
