#include <iostream>

using namespace std;

int prime_numbers (int number)
{
    if(number < 2)
    {
        return -1;
    }

    for(int n = 2; n < number; n++)
    {
        if(number % n == 0) return 0;
    }

    return 1;
}

int main()
{
    int numb;
    while(cin >> numb)
    {
        int pn = prime_numbers(numb);

        if(pn == 1)
        {
            cout << "YES" << endl;
        }
        else if(pn == 0)
        {
            cout << "NO" << endl;
        }
        else
        {
            cout << "N/A" << endl;
        }
    }



    return 0;
}
