#include <iostream>

using namespace std;


int main()
{
     int number, max =0, rem;

    cout << "Enter a number: ";
    cin >> number;

    while ( number != 0)
    {
        rem = number % 10;
        if(rem > max)
       {
            max = rem;
       }
       number = number / 10;
    }

    cout << "The largest digit in that integer is: " << max << endl;

    return 0;
}
