template<typename Object>
class VectorArray
{
public:
	VectorArray(int initialCapacity = 20)
	{
		capacity = initialCapacity;
		size = 0;
		array = new Object[capacity];
	}

	int size() const
	{
		return size;
	}

	bool isEmpty() const
	{
		return size == 0;
	}

	Object& elementAtRank(int r)
	{
		return array[r];
	}

	void replaceAtRank(int r, const Object& element)
	{
		array[r] = element;
	}

	void removeAtRank(int r)
	{
		for (int i = r; i < size - 1; i++)
		{
			array[i] = array[i + 1]; // ne trqbva li arra[i + 1] = array[i]?
			size--;
		}
	}

	void insertAtRank(int r, const Object& element)
	{
		if (size == capacity)
		{
			overflow();
		}
		for (int i = size - 1; i >= r; i--) //cikulut ne mi e mnogo qsen.
		{
			array[i + 1] = array[i]; // ne trqbva li arra[i] = array[i + 1]?
			array[r] = e;
			size++;
		}
	}

protected:
	void overflow() //zashto e protected?
	{
		capacity *= 2;
		Object& bArray = new Object[capacity];
		for (int i = 0; i < size; i++)
		{
			bArray[i] = array[i];
		}
		delete[]array;
		array = bArray; //ne trqbva li bArray = array tui kato bArray kopira stoinostite ot array?
	}
private:
	int capacity;
	int size;
	Object* array;
};