/***
FN:F00000
PID:2
GID:2
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

class Shape
{
public:
	virtual double area() = 0;
	virtual double parameter() = 0;
};

class Triangle : public Shape
{
private:
	double side1;
	double side2;
	double side3;
	double parameter = side1 + side2 + side3;
	double semi = parameter / 2;
	double area = sqrt(semi * (semi - side1) * (semi - side2) * (semi - side3));
public:
	Triangle(double side1, double side2, double side3) : Shape()
	{
	}

	double parameter(double s1, double s2, double s3, double param)
	{
		side1 = s1;
		side2 = s2;
		side3 = s3;
		parameter = param;
		
		return parameter;
	}
	
	double area(double s1, double s2, double s3, double param, double ar)
	{
		side1 = s1;
		side2 = s2;
		side3 = s3;
		area = ar;

		return area;
	}
};

class Circle : public Shape
{
private:
	double radius;
	double pie = 3.14;
	double parameter = 2 * radius * pie;
	double area = pie * pow (radius, 2);
public:
	Circle(double radius, double pie) : Shape()
	{
	}

	double parameter (double rad, double p, double param)
	{
		radius = rad;
		pie = p;
		parameter = param;

		return parameter;
	}

	double area(double rad, double p, double ar)
	{
		radius = rad;
		pie = p;
		area = ar;

		return area;
	}
};


int main()
{
	
	
	int number;
	double side1, side2, side3;
	double radius;
	double pie = 3.14;
	string figure;

	cin >> number;

	vector <Shape*> v;

	for (int i = 0; i < number; i++)
	{
		cin >> figure;
		if (figure == "t")
		{
			
			cin >> side1 >> side2 >> side3;

			v[i] = new Triangle(side1, side2, side3);

			cout << "t " << side1 << side2 << side3 << endl;
		}
		else if (figure == "c")
		{
			cin >> radius;
			
			v[i] = new Circle(radius, pie);
			
			cout << "c " << radius << endl;
		}
	}

	cout << " you want a or p?" << endl;

	string type;

	cin >> type;

	if (type == "a")
	{
		v[2]->area();
	}
	else if (type == "p")
	{
		v[2]->parameter();
	}
	system("pause");
	return 0;
}