#include <iostream>
#include <climits>

using namespace std;

int main()
{
int min_num = INT_MAX;  //  2^31-1
int max_num = INT_MIN;  // -2^31
int input;
while (!cin.eof()) {
    cin >> input;
    min_num = min(input, min_num);
    max_num = max(input, max_num);
}
cout << "min: " << min_num;
cout << "max: " << max_num;
return 0;

}
