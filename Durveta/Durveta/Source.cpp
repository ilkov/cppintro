#include <iostream>

class TreeNode
{
	TreeNode(int);

	TreeNode *left;
	TreeNode *rigth;
	int value;
};

class Tree
{
	Tree();

	void insert(int);
	void find(int);
	void remove(int);
	void inorder();

	TreeNode *root;
	int count;
	void inorder(TreeNode*);
};