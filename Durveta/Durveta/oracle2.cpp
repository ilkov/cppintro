#include <iostream>
#include <string> 
#include <fstream>
#include <vector>
using namespace std;

class CNode {
public:
	CNode(string str) {
		m_Text = str;
		m_pNo = NULL;
		m_pYes = NULL;
	}

	string m_Text;
	CNode* m_pNo;
	CNode* m_pYes;
};


class COracle {
public:
	COracle()
	{
		m_pStart = NULL;
	}
	COracle(string str) {
		m_pStart = new CNode(str);
	}

	CNode* m_pStart;

	int save(string out_file);

	int load(string in_file);

	void newGame();

private:
	void saveNode(CNode* pTmp, int n, vector<string>& out_data);
	void loadNode(CNode*& pTmp, int n, vector<string>& in_data);

};

void COracle::newGame() {

	string ans;
	string animal;
	string question;

	CNode* pTmp = NULL;

	cout << "Think about an animal. Ready? (Y): ";
	cin >> ans;

	pTmp = m_pStart;


	while (pTmp->m_pNo != NULL && pTmp->m_pYes != NULL) {
		cout << pTmp->m_Text << " (Y/N): ";
		cin >> ans;

		if (ans == "Y" || ans == "y") {
			pTmp = pTmp->m_pYes;
		}
		else {
			pTmp = pTmp->m_pNo;
		}
	}

	if (pTmp->m_pNo == NULL && pTmp->m_pYes == NULL) {
		cout << "You think about " << pTmp->m_Text << "? (Y/N): ";
		cin >> ans;

		if (ans == "Y" || ans == "y") {
			cout << endl;
			cout << "I win!!!" << endl << endl;
		}
		else {
			cout << "Tell me your animal: ";
			cin >> animal;

			cout << "Give me a question (a " << pTmp->m_Text << " is NOT but a " << animal << " is): ";
			cin.ignore();
			getline(cin, question);

			pTmp->m_pNo = new CNode(pTmp->m_Text);
			pTmp->m_pYes = new CNode(animal);
			pTmp->m_Text = question;

			cout << endl;
			cout << "You WIN!" << endl << endl;;
		}
	}

}

void COracle::saveNode(CNode* pTmp, int n, vector<string>& out_data)
{
	if (pTmp == NULL)
	{
		return;
	}

	if (out_data.size() < n + 1)
	{
		out_data.resize(n + 1);
	}
	out_data[n] = pTmp->m_Text;

	//out_data.insert(out_data.begin() + n, pTmp->m_Text);
	saveNode(pTmp->m_pNo, 2 * n, out_data);
	saveNode(pTmp->m_pYes, 2 * n + 1, out_data);
}

int COracle::save(string out_file)
{
	vector<string> out_data(1, "N/A");
	
	CNode* pTmp = m_pStart;
	int n = 1;

	saveNode(pTmp, n, out_data);
	out_data[n] = pTmp->m_Text;

	ofstream out_f(out_file.c_str());	//c_str() - vrushta masiv ot charove, koito se sudurja v stringa

	if (out_f.is_open())
	{
		for (int i = 0; i < out_data.size(); i++)
		{
			out_f << out_data[i] << endl;
		}
		out_f.close();

	}
	else
	{
		return -1;
	}
}

int COracle::load(string in_file)
{
	string line;
	vector<string> in_data;
	ifstream in_f(in_file.c_str());
	if (in_f.is_open())
	{
		getline(in_f, line);
		while (!in_f.eof())
		{
			getline(in_f, line);
			in_data.push_back(line);
		}
		in_f.close();
	}
	else
	{
		return -1;
	}

	loadNode(m_pStart, 1, in_data);
}
void COracle::loadNode(CNode*& pTmp, int n, vector<string>& in_data)
{
	if (n > in_data.size())
	{
		return;
	}

	if (in_data[n].empty())
	{
		return;
	}

	pTmp = new CNode(in_data[n]);
	loadNode(pTmp->m_pNo, 2 * n, in_data);
	loadNode(pTmp->m_pYes, 2 * n + 1, in_data);
}

int main() {
	
	string ans;

	COracle* pOracle = new COracle;
	if (-1 == pOracle->load("in.txt"))
	{
		delete pOracle;
		pOracle = new COracle("penguin");
	}
	pOracle = new COracle("penguin");


	do {
		pOracle->newGame();

		cout << "Do you want to play again? (Y/N): ";
		cin >> ans;
		cout << endl << endl;
	} while (ans == "Y" || ans == "y");

	cout << "Bye, bye!";
	pOracle->save("out.txt");
}