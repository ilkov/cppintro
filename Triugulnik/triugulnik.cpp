#include <iostream>

using namespace std;

int main()
{
    double a, b, c;

    cout << "Enter side a: " << "\n";
    cin >> a;

    cout << "Enter side b: " << "\n";
    cin >> b;

    cout << "Enter side c: " << "\n";
    cin >> c;

    double perimeter = (a + b + c);

    if ((a < b + c) && (b < a + c) && (c < a + b)) {
        cout << "The perimeter of the triangle is: " << perimeter;
    }
    else {
        cout << "Such a triangle doesn't exist! ";
    }

    return 0;
}
