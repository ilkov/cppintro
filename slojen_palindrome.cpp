#include <string>
#include <iostream>

using	namespace std;

bool IsPalindrome(std::string str)
{
	/////////////////////////////////////////////////////
	///  Removes Punctuation, spaces, converts to lowercase

	string	tempString;

	for (int i=0, j=0;i<str.length();i++)
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
		{
			tempString.append(1,str[i] + 0x20);		//make Lowercase by adding 0x20
		}
		else if (str[i] >= 'a' && str[i] <= 'z')
		{
			tempString.append(1,str[i]);
		}
	}

	//////////////////////////////////////////////////////
	///	Does Comparison

	int	len = tempString.length();

	for (int i=0;i<len/2;i++)
	{
		if (tempString[i] != tempString[len-i-1])
		{
			return(false);
		}
	}

	return(true);


}
int main()
{
	string	strIn;

	cout<<"please enter a word phrase\n";
	getline(cin,strIn);

	if (IsPalindrome(strIn))
		cout<<"it is a palindrome";
	else
		cout<<"it is not a palindrome";
}
