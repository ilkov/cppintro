# include <iostream>

using namespace std;

int main()
{
    cout << "How many pennies do you have? ";
    int pennies;
    cin >> pennies;

    cout << "How many dimes do you have? ";
    int dimes;
    cin >> dimes;

    double total = pennies * 0.01 + dimes * 0.10;

    cout << "Total value = " << total << "\n";
    return 0;
}
