#include <iostream>
#include <vector>

using namespace std;

void sum(vector <int> v)
{
    int sum =0;
    for(int i = 0; i<v.size(); i++)
            {
                sum += v[i];
            }
            cout << sum << endl;
}

void avg(vector <int> v)
{
    double avg;
    int sum =0;
    int countt = 0;
    for(int i = 0; i<v.size(); i++)
            {
                countt++;
                sum += v[i];
                avg = (double)sum / countt;
            }
            cout << avg << endl;
}

int main()
{
    int n;
    cin >> n;

    vector <int> v;
    v.resize(n);
    for(int i=0; i<n; i++)
    {
        cin >> v[i];
    }
    string cmd;
    while (cin >> cmd)
    {
        if (cmd == "sum")
        {
            sum(v);
        }
        if (cmd == "avg")
        {
            avg(v);
        }
    }
    return 0;
}
