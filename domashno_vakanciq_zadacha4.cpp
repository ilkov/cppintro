#include <iostream>
#include <string>

using namespace std;

string only_uc(const string &str)
{
    string result;
    int length = str.length();
    for(int i=0; i<length; i++)
    {
        char c = str[i];
        if(isupper(c))
        {
            result += c;
        }
    }
    return result;
}

int main()
{
    string str;
    getline(cin, str);
    string result = only_uc(str);
    cout << result << endl;

    return 0;
}
