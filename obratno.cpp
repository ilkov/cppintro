#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str;

    cout << "enter a string: " << endl;
    cin >> str;

    str = (string(str.rend(), str.rbegin()));

    cout << str;

    return 0;
}
