#include <iostream>

using namespace std;

int main()
{
    char operation;
    double operand1, operand2;

    cout << "Enter your operation (+/ -/ x/ :): " << "\n";
    cin >> operation;

    if ((operation != '+') && (operation != '-') && (operation != 'x') && (operation != ':')) {
        cout << "Your operation is invalid! ";
    }
    else {

    cout << "Enter your first operand: ";
    cin >> operand1;

    cout << "Enter your second operand: ";
    cin >> operand2;

    double totalP = (operand1 + operand2);
    double totalM = (operand1 - operand2);
    double totalX = (operand1 * operand2);
    double totalD = (operand1 / operand2);

    switch (operation) {
        case '+': cout << operand1 << " " << "+ " << operand2 << " " << "is " << totalP;
        break;
        case '-': cout << operand1 << " " << "- " << operand2 << " " << "is " << totalM;
        break;
        case 'x': cout << operand1 << " " << "* " << operand2 << " " << "is " << totalX;
        break;
        case ':': cout << operand1 << " " << "/ " << operand2 << " " << "is " << totalD;
        break;
    }
    }

    return 0;
}
