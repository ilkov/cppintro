﻿#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

class Warrior
{
private:
    string name;
    int hitpoints;
    int defence;
    int attackModifier;
public:
    Warrior(string name, int hp, int defence, int attackModifier) : name(name), hitpoints(hp), defence(defence), attackModifier(attackModifier)
    {
    }

    const string & getName() const
    {
        return name;
    }

    void die(){
        cout << name << "drops dead" << endl;
    }

    virtual void attack (Warrior *other)
    {
        cout << name << " hits " << other->name << endl;
        int damage = 1 + rand() % attackModifier;
        other->inflictDamage(damage);
    }

    void inflictDamage(int damage)
    {
        int receiveDamage = max(0, damage - defence);

        cout << this->name << " receives " << receiveDamage << " damage " << endl;
        hitpoints -= receiveDamage;
        if (hitpoints <= 0)
            die();
    }

    bool isAlive()
    {
        return hitpoints > 0;
    }
};

class Archer : public Warrior
{
public:
    Archer(string name, int hp, int attackModifier) : Warrior(name, hp, 0, attackModifier)
    {

    }
    void attack (Warrior *other)
    {
        Warrior::attack(other);
        Warrior::attack(other);
    }
};

int main()
{
    Archer *w1 = new Archer("Legolas", 10, 50);
    Warrior *w2 = new Warrior("Gimli", 20, 2, 5);
    //Warrior *w3 = new Warrior("Balrog", 50, 3, 10);

    while (w1->isAlive() && w2->isAlive())
    {
        int luck = rand() % 2;
        if (luck == 0)
            w1->attack(w2);
        else
            w2->attack(w1);
    }

    Warrior *winner = w1->isAlive() ? w1 : w2;
    cout << "And the winner is " << winner->getName() << endl << "---" << endl;


	return 0;
}
