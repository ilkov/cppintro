#include <iostream>

using namespace std;

int even_numbers(int numb)
{
    int countt2 = 0;
    int countt = 0;
    while (numb != 0)
    {
        int rem;
        rem = numb % 10;
        if(rem % 2 == 0)
        {
            countt++;
        }
        else
        {
           countt2++;
        }
        numb = numb / 10;
    }
    return countt;
}

int main()
{
    int nc;
    cin >> nc;
    while(nc--)
    {
        int n;
        cin >> n;
        int en = even_numbers(n);
        cout << en << endl;
    }


    return 0;
}
