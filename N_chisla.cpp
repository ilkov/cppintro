#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

    double numbers;
    int numbersCount;

    cin >> numbersCount;

    double sum = 0;

    for(int i = 0; i < numbersCount; i++)
    {
        cin >> numbers;
        sum += numbers;
    }
    double avg = sum / numbersCount;

    cout << fixed << setprecision(2) << avg;

    return 0;
}
