#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <fstream>

using namespace std;

bool areStringEqual(char * a, char * b);

class Cryptor
{
private:
	char key;
	char encryptChar(char c)
	{
		int keyOffset = islower(key) ? key - 'a' : key - 'A';
		int num = islower(c) ? c - 'a' : c - 'A';
		num = (num + keyOffset) % 26;
		return islower(c) ? 'a' + num : 'A' + num; 
	}
public:
	Cryptor(char key) : key(key)
	{
	}
	void encrypt(istream &in, ostream &out)
	{
		char c;
		while (in.get(c))
		{
			if (isalpha(c))
				c = encryptChar(c);
			out.put(c);
		}
	}
};

int main(int argc, char * argv [])
{
	char key;
	char *inFile = NULL;
	char *outFile = NULL;
	char * currentArgumentName = NULL;
	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			currentArgumentName = argv[i];
			continue;
		}
		else if (currentArgumentName != NULL)
		{
			if (areStringEqual(currentArgumentName, "-k"))
				key = argv[i][0];
			else if (areStringEqual(currentArgumentName, "-e"))
				inFile = argv[i];

			currentArgumentName = NULL;
		}
		else
			outFile = argv[i];
	}

	ifstream in;
	in.open(inFile);
	ofstream out;
	out.open(outFile);
	Cryptor c(key);
	c.encrypt(in, out);
	in.close();
	out.close();


	//cout << key << endl << inFile << endl << outFile << endl;
	return 0;
}

bool areStringEqual(char * a, char * b)
{
	int len = strlen(a);
	if (len != strlen(b))
		return false;
	for (int i = 0; i < len; i++)
	{
		if (a[i] != b[i])
			return false;
	}

	
	return true;
}
