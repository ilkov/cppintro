#include <iostream>
#include "ArrayDeque.h"


int main()
{
	ArrayDeque <int> numbers(100);

	try
	{
		int num1 = 6;
		int num2 = 7;
		int num3 = 5;
		int num4 = 8;
		int num5 = 11;

		numbers.insertFirst(num1);
	
		numbers.insertFirst(num2);
		
		numbers.insertFirst(num3);

		numbers.insertLast(num4);

		numbers.insertLast(num5);
		
		cout << "First: " << numbers.first() << endl;
		cout << "Last: " << numbers.last() << endl;
		cout << "Size: " << numbers.size() << endl;
		
		numbers.removeFirst();
		cout << "First: " << numbers.first() << endl;

		numbers.removeLast();
		cout << "Last: " << numbers.last() << endl;

		cout << "Size: " << numbers.size() << endl;
		
	}
	catch (EmptyDequeException e)
	{
		cout << e.getMessage();
		return 1;
	}
	system("pause");

	return 0;
}