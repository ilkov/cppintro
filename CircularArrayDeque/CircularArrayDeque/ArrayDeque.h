#include "Exception.h"

class FullDequeException : public RuntimeException
{
public:
	FullDequeException() : RuntimeException("Deque is full") {};
};

class EmptyDequeException : public RuntimeException
{
public:
	EmptyDequeException() : RuntimeException("Deque is empty") {};
};

template<typename Object>
class ArrayDeque
{
public:
	ArrayDeque(int cap = capacity)
	{
		cArray = new Object[capacity];
		front = 0;
		end = 0;
		arraySize = 0;
		capacity = cap;
	}

	int size()
	{
		return arraySize;
	}

	bool isEmpty()
	{
		return (arraySize == 0);
	}

	Object first()
	{
		if (isEmpty())
		{
			throw new EmptyDequeException();
		}
		return cArray[(front - 1) % capacity];
	}

	Object last()
	{
		if (isEmpty())
		{
			throw new EmptyDequeException();
		}
		return cArray[(end + 1) % capacity];
	}

	void insertFirst(Object& element) throw (FullDequeException)
	{
		if (arraySize == capacity)
		{
			throw new FullDequeException();
		}
		cArray[front] = element;
		front = (front + 1) % capacity;
		arraySize++;
	}

	void insertLast(Object& element) throw(FullDequeException)
	{
		if (arraySize == capacity)
		{
			throw new FullDequeException();
		}
		cArray[end] = element;
		end = (end - 1) % capacity;
		arraySize++;
	}

	Object removeFirst() throw(EmptyDequeException)
	{
		if (isEmpty())
		{
			throw new EmptyDequeException();
		}
		front = (front - 1) % capacity;
		arraySize--;
		return cArray[front];
	}

	Object removeLast() throw(EmptyDequeException)
	{
		if (isEmpty())
		{
			throw new EmptyDequeException();
		}
		end = (end + 1) % capacity;
		arraySize--;
		return cArray[end];
	}

	~ArrayDeque()
	{
		delete[] cArray;
	}

private:
	Object* cArray;
	int front;
	int end;
	int arraySize;
	int capacity = 100;
};