#include <string> 
using namespace std;

class RuntimeException {
public:
	RuntimeException(const string& err) { errorMsg = err; }
	string getMessage() const { return errorMsg; }

private:
	string errorMsg;

};