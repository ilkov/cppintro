#include <iostream>

using namespace std;

int main()
{
    cout << "Enter a ";
    double a;
    cin >> a;

    cout << "Enter b ";
    double b;
    cin >> b;

    cout << "a + b = " << (a + b) << "\n";

    cout << "a - b = " << (a - b) << "\n";

    cout << "a * b = " << (a * b) << "\n";

    cout << "a / b = " << (a / b) << "\n";

    return 0;
}
