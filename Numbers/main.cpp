#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int cnt10, cnt20, cnt50;
    double value10 = 0.1, value20 = 0.2, value50 = 0.5;

    cout << "Enter 10s: ";
    cin >> cnt10;

    cout << "Enter 20s: ";
    cin >> cnt20;

    cout << "Enter 50s: ";
    cin >> cnt50;

    double sum = value10 * cnt10 + value20 * cnt20 + value50 * cnt50;

    cout << "You have " << fixed << setprecision(2) << sum << "BGN " << endl;

    return 0;
}
