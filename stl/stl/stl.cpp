#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <set>
using namespace std;

int fib(int n)
{
	queue<int> w;
	w.push(1);
	w.push(1);
	w.push(1);

	int result = 1;
	if (n < 4)
		return 1;
	
	for (int i = 4; i <= n; i++)
	{
		int next = w.front(); w.pop() /*+ w.back()*/;
		next += w.back() * 3 - w.front() * 2;
		w.push(next);
		w.pop();
	}

	return w.back();
}

int main()
{
	//cout << fib(8) << endl;
	set<int> s;
	int n;
	while (cin >> n)
	{
		s.insert(n);
	}
	for (set<int>::iterator it = s.begin(); it != s.end(); it++)
	{
		cout << *it << endl;
	}
	system("pause");
	return 0;
}