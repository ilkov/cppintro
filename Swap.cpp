#include <iostream>
#include <utility>

using namespace std;

double swapping(double &a, double &b)
{
    swap(a, b);
    cout << a << " " << b << endl;
}

int main()
{
    double c, d;
    cin >> c >> d;
    double swapp = swapping(c, d);

    return 0;
}
