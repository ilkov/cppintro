/***
FN:F73855
PID:2
GID:3
*/

#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

class Worker
{
private:
	int hours;
	double salary;
public:
	Worker(int hours, double salary) : hours(hours), salary(salary)
	{
	}
	virtual double getSalaryS(int hours, double salary);
};

class SalariedWorker : public Worker
{
private:
	string name;
	string type;
public:
	SalariedWorker(int hours, double salary) : Worker(hours, salary)
	{
	}

	void setTypeS(string stype)
	{
		type = stype;
	}

	const string & getTypeS() const
	{
		return type;
	}

	void setNameS(string cname)
	{
		name = cname;
	}

	const string & getNameS() const
	{
		return name;
	}

	double getSalary(int hours, double salary)
	{
		if (hours == 40)
		{
			return hours * salary;
		}
		else if (hours < 40)
		{
			return (hours * salary) / 2;
		}
		else if (hours > 40)
		{
			return (hours * salary) * 1.2;
		}

		SalariedWorker* findPersonS(const vector<SalariedWorker*>& persons, string name)
		{
			for (int i = 0; i < persons.size(); i++)
			{
				if (persons[i]->getNameS() == name)
					return persons[i];
			}
			return NULL;
		}
};

class Consultant : public Worker
{
private:
	string name;
	string type;
public:
	Consultant(int hours, double salary) : Worker(hours, salary)
	{
	}

	void setTypeC(string stype)
	{
		type = stype;
	}

	const string & getTypeC() const
	{
		return type;
	}

	void setNameC(string cname)
	{
		name = cname;
	}

	const string & getNameC() const
	{
		return name;
	}

	double getSalary(int hours, double salary)
	{
		if (hours > 20)
		{
			cout << "Too much hours!" << endl;
		}
		else
		{
			return hours * salary;
		}
	}

	Consultant* findPersonC(const vector<Consultant*>& persons, string name)
	{
		for (int i = 0; i < persons.size(); i++)
		{
			if (persons[i]->getNameC() == name)
				return persons[i];
		}
		return NULL;
	}
};

int main()
{
	int n;
	string type;
	string name;
	int hours;
	int hoursw;
	double salary;
	vector <Worker*> workers;
	int q;

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> name;

		workers[i] = new Worker(hours, salary);
	}

	cin >> q;

	system("pause");
	return 0;
}