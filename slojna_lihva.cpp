#include <iostream>
#include <cmath>

using namespace std;

 double future_value(double deposit, double rate, int years)
    {
        double future_balance = deposit * pow(1 +  rate / 100, years);
        return future_balance;
    }


int main()
{


    cout << "Enter your deposit amount: " << endl;
    double d;
    cin >> d;

    cout << "Enter the yearly rate: " << endl;
    double r;
    cin >> r;

    cout << "Enter the number of years the amount has been in the bank: " << endl;
    int y;
    cin >> y;

    double balance = future_value(d, r, y);

    if (y > 1)
    {
        cout << "Your amount after " << y << " years is: " << balance << endl;
    }
    if (y <= 1)
    {
        cout << "Your amount after " << y << " year is: " << balance << endl;
    }
    if (y < 0)
    {
        cout << "Your amount can't stay negative years in the bank! " << endl;
    }


    return 0;
}
