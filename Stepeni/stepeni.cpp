#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double number;

    cout << "Enter your number: " << "\n";
    cin >> number;

    cout << number << "^1 ="<< pow(number, 1) << ", " << number << "^2 = " << pow(number,2) << ", " << number << "^3 = " << pow(number,3) << ", " << number << "^4 = " << pow(number,4) << ", " << number << "^5 = " << pow(number,5);

    return 0;
}
