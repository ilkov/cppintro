#include <iostream>

using namespace std;

int even_numbers(int number)
{
    int countt = 0;
    while (number != 0)
    {
        int rem;
        rem = number % 10;
        if(rem % 2 == 0)
        {
            countt++;
        }
    }
    return countt;
}

int main()
{
    int nc;
    cin >> nc;
    while(nc--)
    {
        int n;
        cin >> n;
        int en = even_numbers(n);
        cout << en << endl;
    }


    return 0;
}
