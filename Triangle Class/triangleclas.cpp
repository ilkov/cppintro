#include <iostream>
#include <string>

using namespace std;

class Triangle
{
private:
    double a;
    double b;
    double c;
public:
    Triangle(double _a, double _b, double _c)
    {
        a = _a;
        b = _b;
        c = _c;
    }

    bool exists()
    {
        return a < b+c && b < a+c && c < b+a;
    }

    double get_perimeter()
    {
        return a+b+c;
    }

    double get_area()
    {
       double sqrt;
        double sp = get_perimeter() /2;
        return sqrt(sp*  (sp - a) * (sp - b) * (sp - c));
    }
};

//for homework

int main()
{
    double a, b, c;
    cin >> a >> b >> c;

    Triangle t(a, b, c);

    if(!t.exists())
    {
        cout << "No such triangle!" << endl;
        return 0;
    }

    cout << t.get_perimeter() << endl;
    cout << t.get_area() << endl;

    return 0;
}
