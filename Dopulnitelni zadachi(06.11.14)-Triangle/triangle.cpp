#include <iostream>
#include <string>

using namespace std;

class Triangle
{
private:
    double a;
    double b;
    double c;
public:
   Triangle()
   {
   }

    double get_perimeter()
    {
        double p = a + b + c;
        return p;
    }
    double get_area()
    {
        double ha, hb, hc;
        double area = (((a * ha) / 2) && ((b * hb) / 2) && ((c * hc) / 2));
        return area;
    }
};
//for homework

int main()
{
    double a, b, c;
    cin >> a >> b >> c;

    Triangle t(a, b, c);

    if(!t.exists())
    {
        cout << "No such triangle!" << endl;
        return 0;
    }

    cout << t.get_perimeter() << endl;
    cout << t.get_area() << endl;

    return 0;
}
