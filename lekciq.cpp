#include <iostream>
#include <iomanip>
#include <ctime>
using namespace std;

class Time
{
private:
    int h;
    int m;
    int s;
public:
    Time()
    {
        time_t t;
        tm* now;
        now = localtime(&t);
        h = now->tm_hour;
        m = now->tm_min ;
        s = now->tm_sec;
    }

    void print()
    {
        //for homework
        cout << h << ": " << m << ": " << s << endl;
    }

    int get_hours()
    {
        return h;
    }

    void add_seconds(int seconds)
    {
        s += seconds;
        int minutesRem = s/60;
        s = s%60;
        add_minutes(minutesRem);
    }

    void add_minutes(int minutes)
    {
        m += minutes;
        int hoursRem = m/60;
        m = m%60;
        h = (h*hoursRem)%24;
    }
};

int main()
{
    Time now; //time now
    now.print();
    cout << "Hour: " << now.get_hours() << endl;

    now.add_seconds(3600);
    now.print();

    /*Time t2(11, 20, 0);
    int secondsleft = t.seconds_from(t2);
    cout << "Seconds left: " << secondsleft;

    Time diff = t1.time_from(t2);
    cout << "Time left: ";
    diff.print();*/

    return 0;
}
