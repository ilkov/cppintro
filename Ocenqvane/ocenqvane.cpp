#include <iostream>

using namespace std;

int main()
{
    double grade1, grade2;

    cout << "Enter your grades: " << "\n";
    cin >> grade1;

    if (grade1 != 2 && grade1 != 3 && grade1 != 4 && grade1 != 5 && grade1 != 6) {
        cout << "Your grade is not valid in Bulgaria! ";
        return 0;
    }
    else {
        cout << "Enter your second grade: " << "\n";
        cin >> grade2;
    }
    if (grade2 != 2 && grade2 != 3 && grade2 != 4 && grade2 != 5 && grade2 != 6) {
        cout << "Your second grade is not valid in Bulgaria! ";
    }
    else {
        cout << "Your average grade is: " << (grade1 + grade2)/2;
    }
    return 0;
}
