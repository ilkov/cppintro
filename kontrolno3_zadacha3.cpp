#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>

using namespace std;

int main()
{
    vector<double> v;
    int numbers_count;
    double input;
    string cmd;
    int position;

    cin >> numbers_count;

    for(int i = 0; i < numbers_count; i++)
    {
        cin >> input;
        v.push_back(input);
    }
    while (cin >> cmd)
    {
        if(cmd == "insert")
        {
            cin >> position;
            cin >> input;
            v.push_back(input);
            v.insert(v.begin() + position, input);
            for(int j=0; j<v.size() - 1; j++)
         {
            cout << v[j] << ' ';
         }
        }
         else if(cmd == "delete")
        {
           cin >> position;
           v.erase(v.begin()+position);
           for(int j=0; j<v.size(); j++)
         {
            cout << v[j] << ' ';
         }
        }

        else if(cmd == "print")
        {
             for(int j=0; j<v.size(); j++)
         {
            cout << v[j] << ' ';
         }
        }
        else if(cmd == "exit")
        {
            return 0;
        }
    }

    return 0;
}
