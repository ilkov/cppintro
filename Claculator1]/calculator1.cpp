#include <iostream>

using namespace std;

int main()
{
    double operand1, operand2;
    char operation;

    cout << "Enter operation(a = +,s = -,m = * or p = /): " << "\n";
    cin >> operation;

    cout << "Enter your first operand: " << "\n";
    cin >> operand1;

    cout << "Enter your second operand: " << "\n";
    cin >> operand2;

    double totalA = (operand1 + operand2);
    double totalS = (operand1 - operand2);
    double totalM = (operand1 * operand2);
    double totalP = (operand1 / operand2);

    switch (operation) {
        case 'a': cout << operand1 << " " << "+ " << operand2 << " " << "is " << totalA;
        break;
        case 's': cout << operand1 << " " << "- " << operand2 << " " << "is " << totalS;
        break;
        case 'm': cout << operand1 << " " << "* " << operand2 << " " << "is " << totalM;
        break;
        case 'p': cout << operand1 << " " << "/ " << operand2 << " " << "is " << totalP;
        break;
    }


    return 0;
}
