#include <iostream>
#include <string>

using namespace std;

string process(const string &str)
{
    string result;
    int len = str.length();
    for(int i=0; i<len; i++)
    {
        char c = str[i];
        if(islower(c))
        {
            result += c;
        }
    }
    return result;
}

int main ()
{
    int n;
    cin >> n;
    while(n--)
    {
        string str;
        cin >> str;
        string result = process(str);
        cout << result << endl;
    }

    return 0;
}
