#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
class Vec2
{
public:
	Vec2(T x = 0.0f, T y = 0.0f) : x(x), y(y)
	{

	}

	T get_x() const
	{
		return x;
	}

	T get_y() const
	{
		return y;
	}

	Vec2<T> operator+(Vec2<T> v)
	{
		return Vec2<T>(x + v.x, y + v.y);
	}

	bool operator<(Vec2<T> v)
	{
		return x < v.x;
	}
private:
	T x, y;
};

template <typename T>
void Sort(vector<T>& v)
{
	if (v[0] < v[1])
		return;
}

template <typename Ty>
ostream & operator<<(ostream& os, Vec2<Ty> v)
{
	os << "(" << v.get_x() << ", " << v.get_y() << ")";
	return os;
}

int main()
{
	Vec2<float> a(2.4, 9);
	Vec2<float> b(10, 20);
	vector< Vec2<float> > v(2);
	Sort(v);
	cout << a << endl << b << endl;
	cout << a + b << endl;

	return 0;
	system("pause");
}