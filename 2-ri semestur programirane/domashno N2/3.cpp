/***
FN:F73855
PID:3
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>
using namespace std;
 
int main(int argc, char* argv[])
{
	ifstream inp_data;
	inp_data.open(argv[1]);
	if (inp_data.fail())
	{
		cout << "Error" << endl;
		return -1;
	}

	ofstream out_data;
	out_data.open(argv[2]);
	if (out_data.fail())
	{
		cout << "Error " << endl;
		return -1;
	}

	
	char ch;
	while (inp_data.get(ch)) {
		if (isdigit(ch))
			out_data << ch;
		else if (islower(ch))
			out_data << ch;
		else if (isspace(ch))
			out_data << ch;
	}

	inp_data.close();
	out_data.close();

	return 0;
} 