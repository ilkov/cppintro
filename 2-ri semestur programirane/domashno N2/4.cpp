/***
FN:F73855
PID:4
GID:1
*/

#include <iostream>

using namespace std;

long iterative(long n) 
{
	if (n < 4)
		return 1;

	long i = n - 2;
	long x1 = 1;
	long x2 = 1;
	long x3 = 1;

	while (i>1)
	{
		long res = (3 * x3) - (2 * x2) + x1;
		x1 = x2;
		x2 = x3;
		x3 = res;
		i--;
	}
	return x3;
}

long row(long n)
{
	
	if (n < 4)
		return 1;
	else
		return  (3 * row(n - 1)) - (2 * row(n - 2)) + (row(n - 3));
}

int main(int argc, char * argv[])
{
	double n;
	int i;
	i = atol(argv[1]);
	
	cout << row(i) << endl << iterative(i)<< endl;

	return 0;
}
