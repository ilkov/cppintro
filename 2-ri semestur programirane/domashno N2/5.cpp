/***
FN:F73855
PID:5
GID:1
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;

void bubbleSort(vector<int>& A);
void selectionSort(vector<int>& A);
void printVector(vector<int>& A);

int main()
{
	int n;
	cin >> n;
	vector<int> A(n);
	for (int i = 0; i < n; i++) cin >> A[i]; cout << endl;

	vector<int> bsCopy(A.begin(), A.end());
	bubbleSort(bsCopy);

	cout << endl;

	vector<int> ssCopy(A.begin(), A.end());
	selectionSort(ssCopy);

	system("pause");
	return 0;
}

void bubbleSort(vector<int>& A)
{
	int n = A.size();
	
	for (int i = 1; i < n - 1; i++)
	{
		if (A[i - 1] > A[i]) {
			swap(A[i - 1], A[i]);
			
			printVector(A);
		}	
	}

	for (int j = 1; j < n - 1; j++)
	{
		if (A[j - 1] > A[j]) {
			swap(A[j - 1], A[j]);
					
			printVector(A);
		}
	}
}

void selectionSort(vector<int>& A)
{
	int i, j;
	int iMin;
	int n = A.size() - 1;

	
	for (j = 0; j < n - 1; j++) 
	{
		iMin = j;
		 
		for (i = j + 1; i < n; i++) 
		{
			
			if (A[i] < A[iMin]) 
			{
				iMin = i;
			}	
		}

		if (iMin != j) 
		{
			swap(A[j], A[iMin]);
		}
		printVector(A);
	}
}

void printVector(vector<int>& A)
{
	for (int i = 0; i < A.size(); i++)
		cout << A[i] << (i == A.size() - 1 ? "\n" : " ");
}