/***
FN:F73855
PID:2
GID:1
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

vector <double> calculate(vector<double> & v, char operation, int constt) 
{
	for (int i = 0; i < v.size(); i++) {
		if (operation == '+') {
			v.at(i) = v.at(i) + constt;
		}
		else if (operation == 'x') {
			v.at(i) = v.at(i) * constt;
		}
		else if (operation == '-') {
			v.at(i) = v.at(i) - constt;
		}
	}
	return v;
}

int main(int argc, char* argv[])
{
	vector<double> v;

	ifstream inp_data;
	inp_data.open(argv[1]);
	if (inp_data.fail())
	{
		cout << "Error: " << argv[1] << endl;
		return -1;
	}

	ofstream out_data;
	out_data.open(argv[2]);
	if (out_data.fail())
	{
		cout << "Error: " << argv[2] << endl;
		return -1;
	}

	double n;
	while (inp_data >> n)
	{
		v.push_back(n);
	}

	calculate(v, argv[3][0], atoi(argv[4]));

	for (int i = 0; i < v.size(); i++) 
	{
		out_data << v.at(i);
		if (i != v.size() - 1)
		{
			out_data << " ";
		}
		else 
		{
			out_data << endl;
		}
	}

	inp_data.close();
	out_data.close();

	return 0;
}