/***
FN:F73855
PID:1
GID:1
*/

#include <iostream>
#include <fstream>
#include <limits>

using namespace std;

int main2(int argc, char * argv[])
{
	ifstream inp_data;
	inp_data.open(argv[1]);
	if (inp_data.fail())
	{
		cout << "Error" << endl;
		return -1;
	}

	double sum = 0;
	int count = 0;
	double avg;
	double max = numeric_limits<int>::min();
	double min = numeric_limits<int>::max();
	while (!inp_data.eof())
	{
		int n;
		inp_data >> n;
		if (min > n)
			min = n;
		if (n > max)
			max = n;
		count++;
		sum += n;
		avg = sum / count;
	}
	cout << "SUM: " << sum << endl << "AVG: " << avg << endl << "MIN: " << min << endl << "MAX: " << max << endl;
	

	inp_data.close();

	return 0;
}