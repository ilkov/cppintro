#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

void bubbleSort(vector <int>&  v);
void print(vector <int>& v);

int main1()
{
	int input;
	vector<int> v;
	int n;
	cin >> n;

	for (int i = 0; i < n; i++)
	{
		cin >> input;
		v.push_back(input);
	}

	bubbleSort(v);

	system("pause");
	return 0;
}

void print(vector <int>& v)
{
	for (int j = 0; j < v.size(); j++)
	{
		cout << v[j] << ' ';
	}
	cout << endl;
}

void bubbleSort(vector <int>& v)
{
		for (int h = 0; h < v.size(); h++)
	{
		if (v[h - 1] > v[h])
			swap(v[h - 1], v[h]);

		print(v);
	}

	print(v);
}