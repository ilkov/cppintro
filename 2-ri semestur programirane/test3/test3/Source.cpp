#include <iostream>
#include <string>
#include <list>
#include <queue>

using namespace std;

void fun3()
{
	throw runtime_error("RTE");
}
void fun2() throw(runtime_error)
{
	//throw string("RTE"); fun3();
	//try { fun3(); }
	//catch (runtime_error e) { throw; }
	//catch (...) {}
	try { fun3(); }
}
void fun1() throw(runtime_error)
{
	fun2();
}

int main()
{
	

		try
		{
			fun1();
		}
		catch (runtime_error e)
		{
			cout << e.what();
		}


	
	
	system("pause");
	return 0;
}