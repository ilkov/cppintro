#include <iostream>
#include <string>

using namespace std;

class Circle
{
private:
    double r;
public:
    Circle(double radius): r(radius)
    {
    }

    double get_radius()
    {
        return r;
    }

    double get_perimeter()
    {
        double pie = 3.14;
        double perimeter = 2 * pie * r;
        return perimeter;
    }
    double get_area()
    {
        double pie = 3.14;
        double area = pie * r * r;
        return area;
    }
};

int main()
{
    double radius;
    cin >> radius;

    Circle c(radius);

    cout << c.get_radius() << "\n" << endl;
    cout << c.get_perimeter() << "\n";
    cout << c.get_area() << "\n";

    return 0;
}
