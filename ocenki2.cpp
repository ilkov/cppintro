#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
    string fname, lname, fnumber;
    double grade1, grade2, grade3, grade4;
    int number_of_students = 0;

    while (number_of_students <= 3)
    {
        cout << "Enter the students first and last name: " << endl;
        cin >> fname >> lname;

        cout << "Enter the students faculty number: " << endl;
        cin >> fnumber;

        cout << "Enter the students four grades: " << endl;
        cin >> grade1 >> grade2 >> grade3 >> grade4;
        if ((grade1 < 2) || (grade1 > 6))
        {
            cout << "That grade is invalid!";
            return 0;
        }
        if ((grade2 < 2) || (grade2 > 6))
        {
           cout << "That grade is invalid!";
           return 0;
        }
        if ((grade3 < 2) || (grade3 > 6))
        {
           cout << "That grade is invalid!";
           return 0;
        }
        if ((grade4 < 2) || (grade4 > 6))
        {
           cout << "That grade is invalid!";
           return 0;
        }
        else
        {
        double avg = (grade1 + grade2 + grade3 + grade4) / 4;
        cout << avg << endl;

        number_of_students++;
        }
    }
    double avg;

    cout << fixed << setprecision(2);

    cout << setw(12) << "Faculty No." << setw(7) <<  "Grade" << endl;
    cout << setw(12) << fnumber << setw(7) <<  avg << endl;


    return 0;
}
