#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

int main()
{
    string fname, lname, fnumber;
    cin >> fname >> lname >> fnumber;

    double grade1, grade2, grade3;
    cin >> grade1 >> grade2 >> grade3;

    double avg = (grade1 + grade2 + grade3)/3;
    cout << fixed << setprecision(2);

    if ( avg >= 2.0 && avg <= 6.0 ) {
        cout << "Your grade is between 2 and 6!\n ";
    }
    else {
        cout << "Your grade is not between 2 and 6!\n ";
    }
    cout << fnumber << ": " << avg << endl;

    return 0;

}
