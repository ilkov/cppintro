#include <iostream>
#include <string>

using namespace std;

int main()
{
    string str;

    cout << "Enter your string to see if it's a palindrome: " << "\n";
    cin >> str;

    if(str == string(str.rbegin(), str.rend()))
    {
        cout << "Yes, " << str << " is a palindrome!";
    }
    else
    {
        cout << "No, " << str << " is not a palindrome!";
    }

    return 0;
}
