#include <iostream>

using namespace std;

int t(int n);
void print(int n);

int main1(int argc, char * argv[])
{
	int n;
	while (cin >> n)
	{
		cout << t(n) << endl;
		print(n);
	}

	system("pause");

	return 0;
}

int t(int n)
{
	if (n < 1)
		return 0;
	else if (n == 1)
		return 1;

	return t(n - 1) + n;
}

void print(int n)
{
	if (n < 1)
		return;
	
	print(n - 1);
	for (int i = 0; i < n; i++)
	{
		cout << "[]" << endl;
	}
}