#include <iostream>
#include <string>

using namespace std;

string only_lc(const string &str)
{
    string result;
    int length = str.length();
    for(int i=0; i<length; i++)
    {
        char c = str[i];
        if(islower(c))
        {
            result += c;
        }
    }
    return result;
}

int main()
{
    string str;
    getline(cin, str);
    string result = only_lc(str);
    cout << result << endl;

    return 0;
}
